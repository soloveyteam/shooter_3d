﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreLabel : MonoBehaviour
{
    private static int score;
    private static Text label;

    public static int Score
    {
        get { return score; }
        set 
        { 
            score = value;
            label.text = "SCORE: " + score.ToString();
        }
    }

    private void Start()
    {
        label = gameObject.GetComponent<Text>();
    }

    private void Update()
    {
        
    }
}
