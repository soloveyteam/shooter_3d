﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : Character
{
    private NavMeshAgent _navMeshAgent;

    [SerializeField] private Transform target;

    [SerializeField] private GameObject bulletPrefab; //Префаб пули

    [SerializeField] private Transform gun; //Пушка

    [SerializeField] private float shootPower = 500.0f; //Сила, прикладываемая к снарядам

    [SerializeField] private float shootDelay;

    [SerializeField] private float bulletDamage = 10.0f;

    [SerializeField] private int pointsPerKill = 25;

    [SerializeField] private GameObject explosionEnemyPrefab;

    private bool seeTarget;

    public Transform Target
    {
        get { return target; }
        set { target = value; }
    }

    private void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        InvokeRepeating("Shoot", 0.0f, shootDelay);
    }

    private void Update()
    {
        CheckTargetVisibility();
        _navMeshAgent.SetDestination(target.position);
    }

    private void Shoot()
    {
        if (seeTarget)
        {
            Vector3 targetDirection = target.position - gun.transform.position;
            targetDirection.Normalize();
            ShootBullet(targetDirection);
            //GameObject newBullet = Instantiate(bulletPrefab, gun.position, gun.rotation) as GameObject;
            //newBullet.GetComponent<Rigidbody>().AddForce(targetDirection * shootPower);
            //newBullet.GetComponent<Damager>().Damage = bulletDamage;
            //Destroy(newBullet, 5);
        }
    }

    private void CheckTargetVisibility()
    {
        Vector3 targetDirection = target.position - gun.transform.position;
        Ray ray = new Ray(gun.transform.position, targetDirection);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform == target)
            {
                seeTarget = true;
                return;
            }
        }
        seeTarget = false;
    }

    private void Destroyed()
    {
        if (explosionEnemyPrefab != null)
        {
            Explosion.Create(transform.position, explosionEnemyPrefab);
        }
        if (Random.Range(0, 100) < 50)
        {
            HealthBonus.Create(transform.position);
        }
        ScoreLabel.Score += pointsPerKill;
    }
}
