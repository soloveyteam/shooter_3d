﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyWayPoints : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    private Transform target;
    public Transform[] wayPoint;
    private int index = 0;

    void Start()
    {
        MoveToWayPoint();
    }

    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        if ((transform.position - playerTransform.position).magnitude < 20.0f)
        {
            navMeshAgent.SetDestination(playerTransform.position);
        }
        else
        {
            navMeshAgent.SetDestination(target.position);
            if ((transform.position - target.position).magnitude < 3.0f)
            {
                index++;
                if (index >= wayPoint.Length)
                {
                    index = 0;
                    MoveToWayPoint();
                }
                else
                {
                    MoveToWayPoint();
                }
            }
        }
    }

    private void MoveToWayPoint()
    {
        target = wayPoint[index];
    }
}
