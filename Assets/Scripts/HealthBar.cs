﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Image healthBar;
    [SerializeField] private Destructable owner;
    private bool rotateBar = true;

    private void Start()
    {
        healthBar = gameObject.GetComponent<Image>();
        if (owner.GetComponent<CharacterController>() != null)
        {
            rotateBar = false;
        }
    }

    private void Update()
    {
        healthBar.fillAmount = Mathf.InverseLerp(0.0f, owner.HitPoints, owner.HitPointsCurrent);
        if (rotateBar)
        {
            transform.forward = transform.position - Camera.main.transform.position;
            //transform.forward = Camera.main.transform.forward;
        }
    }
}
