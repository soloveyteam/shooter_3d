﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructable : MonoBehaviour
{
    [SerializeField] private float hitPoints = 100.0f;

    private float hitPointsCurrent;

    public float HitPointsCurrent
    {
        get { return hitPointsCurrent; }
        set { hitPointsCurrent = value; }
    }

    public float HitPoints
    {
        get { return hitPoints; }
        set { hitPoints = value; }
    }

    void Start()
    {
        hitPointsCurrent = hitPoints;
    }

    public void Hit(float damage)
    {
        hitPointsCurrent -= damage;
        if (hitPointsCurrent <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        BroadcastMessage("Destroyed");
        Destroy(gameObject);
    }
}
