﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Character : MonoBehaviour
{
    [SerializeField] private GameObject bulletPrefab; //Префаб пули
    [SerializeField] private GameObject rocketPrefab; //Перфаб ракетницы
    [SerializeField] private Transform gun; //Пушка
    [SerializeField] private Transform rocketLauncher; //Ракетница
    [SerializeField] private float shootPower = 500.0f; //Сила, прикладываемая к снарядам
    [SerializeField] private float bulletDamage = 10.0f;
    [SerializeField] private float rocketDamage = 20.0f;
    [SerializeField] private float rocketDelay = 2.0f;
    private float rocketDelayCurrent;

    public Transform RocketLauncher
    {
        get { return rocketLauncher; }
        set { rocketLauncher = value; }
    }

    public Transform Gun
    {
        get { return gun; }
        set { gun = value; }
    }

    public GameObject BulletPrefab
    {
        get { return bulletPrefab; }
        set { bulletPrefab = value; }
    }

    public GameObject RocketPrefab
    {
        get { return rocketPrefab; }
        set { rocketPrefab = value; }
    }

    protected void ShootRocket(Vector3 direction)
    {
        if (rocketDelayCurrent <= 0)
        {
            rocketDelayCurrent = rocketDelay;
            GameObject newRocket = Instantiate(RocketPrefab, rocketLauncher.position, rocketLauncher.rotation) as GameObject;
            direction = direction.normalized;
            newRocket.GetComponent<ConstantForce>().force = direction * shootPower / 10;

            Damager rocketBehaviour = newRocket.GetComponent<Damager>();
            rocketBehaviour.Damage = rocketDamage;
            rocketBehaviour.Owner = gameObject;

            //newRocket.GetComponent<Damager>().Damage = rocketDamage;
            Destroy(newRocket, 5);
        }
    }

    protected void UpdateTimer()
    {
        if (rocketDelayCurrent > 0)
        {
            rocketDelayCurrent -= Time.deltaTime;
        }
    }

    protected void ShootBullet(Vector3 direction)
    {
        GameObject newBullet = Instantiate(BulletPrefab, gun.position, gun.rotation) as GameObject;
        direction = direction.normalized;
        newBullet.GetComponent<Rigidbody>().AddForce(direction * shootPower);

        Damager bulletBehaviour = newBullet.GetComponent<Damager>();
        bulletBehaviour.Damage = bulletDamage;
        bulletBehaviour.Owner = gameObject;

        //newBullet.GetComponent<Damager>().Damage = bulletDamage;
        //newBullet.GetComponent<Damager>().Owner = gameObject;
        Destroy(newBullet.gameObject, 5);
    }
}
