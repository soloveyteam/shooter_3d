﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour
{
    [SerializeField] private float damage;
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private float radius;
    [SerializeField] private int explosionPowerCoef = 1000;
    private GameObject owner;

    public GameObject Owner
    {
        get { return owner; }
        set { owner = value; }
    }

    public float Damage 
    {
        get { return damage; }
        set { damage = value; } 
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!GameObject.Equals(collision.gameObject, Owner))
        {
            if (radius > 0.0f)
            {
                CauseExplosionDamage();
            }
            else
            {
                Destructable target = collision.gameObject.GetComponent<Destructable>();
                if (target != null)
                {
                    target.Hit(Damage);
                }
            }

            //Отделяем дым от родителя, но что-то работает не так
            ParticleSystem trail = gameObject.GetComponentInChildren<ParticleSystem>();
            if (trail != null)
            {
                //Destroy(trail.gameObject, trail.startLifetime);
                trail.Stop();
                trail.transform.SetParent(null);
            }

            if (explosionPrefab != null)
            {
                Explosion.Create(transform.position, explosionPrefab);
            }

            Destroy(gameObject);
        }
    }

    private void CauseExplosionDamage()
    {
        Collider[] explosionVictims = Physics.OverlapSphere(transform.position, radius);
        for (int i = 0; i < explosionVictims.Length; i++)
        {
            Vector3 vectorToVictim = explosionVictims[i].transform.position - transform.position;
            float decay = 1 - (vectorToVictim.magnitude / radius);
            Destructable currentVictim = explosionVictims[i].gameObject.GetComponent<Destructable>();
            if (currentVictim != null)
            {
                currentVictim.Hit(Damage * decay);
            }
            Rigidbody victimRigidbody = explosionVictims[i].gameObject.GetComponent<Rigidbody>();
            if (victimRigidbody != null)
            {
                victimRigidbody.AddForce(vectorToVictim.normalized * decay * explosionPowerCoef);
            }
        }
    }
}
