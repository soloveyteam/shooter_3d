﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField] private float spawnDelay;
    [SerializeField] private Enemy enemyPrefab;

    [SerializeField] private List<Enemy> listOfEnemies = new List<Enemy>();

    //private Object[] temp;
    public void Spawn()
    {
        //Enemy newEnemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity) as Enemy;
        int number = Random.Range(0, 5);
        Enemy newEnemy = Instantiate(listOfEnemies[number], transform.position, Quaternion.identity) as Enemy;
        //Enemy newEnemy = Instantiate(temp[number], transform.position, Quaternion.identity) as Enemy;
        newEnemy.Target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Start()
    {
        //listOfEnemies.Add(Resources.Load("Prefabs/Enemies/EnemyGreen") as Enemy);
        //temp = Resources.LoadAll("Prefabs/Enemies/");
        //for (int i = 0; i < temp.Length; i++)
        //{
        //    listOfEnemies.Add(temp[i] as Enemy);
        //}


        InvokeRepeating("Spawn", 0.0f, spawnDelay);
    }
}
